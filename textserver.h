/**
 * hubstatlink.h
 * Erick Veil
 * 2021-08-23
 * Copyright 2021 Erick Veil
 */
#ifndef TEXTSERVER_H
#define TEXTSERVER_H

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QList>
#include <QObject>
#include <QFile>
#include <QByteArray>

#include "staticlogger.h"
#include "universallistener.h"

/**
 * @brief The TextServer class
 * Main class that handles all listeners
 * Establishes a list of listeners for each port in the range.
 * Handles callbacks for listeners
 *
 * Listener recieves data - runs ack callback
 * Ack callback uses file data as ack
 */
class TextServer : public QObject
{
    Q_OBJECT

    int _portRangeStart = 59900;
    int _portRangeEnd = 60000;

    QList<evtools::UniversalListener*> _listenerPool;

    bool _isInit = false;
    QString _filename;
    QByteArray _fileData;

public:
    explicit TextServer(QObject *parent = nullptr);
    ~TextServer();

    void init(QString filename);

private:
    QByteArray _createLocalGetMsg();
    void _initListenerPool();
    void _initListener(int port);
    QByteArray _listenerAckCb();
    QByteArray _createFacDownMsg();
    void _initFileData();

};

#endif // TEXTSERVER_H
