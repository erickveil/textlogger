# textserver

Socket server listens for "anything" then responds with the contents of a
text file.

Run by providing the text file as the only argument.

If no file name is provided, will not run.

Intended to be used as a test endpoint for developing socket applications
that expect a specific response from the servers they connect to.

