// Copyright 2021 Erick Veil

#include "textserver.h"

TextServer::TextServer(QObject *parent) : QObject(parent)
{

}

TextServer::~TextServer()
{
    for (int i = 0; i < _listenerPool.size(); ++i) {
        delete _listenerPool.at(i);
    }
}

void TextServer::init(QString filename)
{
    _filename = filename;
    _initFileData();
    if (_isInit) { return; }
    _initListenerPool();
    _isInit = true;
}

QByteArray TextServer::_createLocalGetMsg()
{
    QString key = "hubStatusList";
    return key.toLocal8Bit();
}

void TextServer::_initListenerPool()
{
    for (int port = _portRangeStart; port <= _portRangeEnd; ++port) {
        _initListener(port);
    }
}

void TextServer::_initListener(int port)
{
    auto listener = new evtools::UniversalListener();
    _listenerPool.append(listener);
    _listenerPool.last()->setPort(port);
    auto ackCb = [&] (QByteArray msg) {
        Q_UNUSED(msg);
        return _listenerAckCb();
    };
    _listenerPool.last()->setAckCallback(ackCb);
    _listenerPool.last()->Name = "Network Listener " + QString::number(port);
    _listenerPool.last()->initConnections();
    _listenerPool.last()->startListener();
}

QByteArray TextServer::_listenerAckCb()
{
    return _fileData;
}

QByteArray TextServer::_createFacDownMsg()
{
    QJsonObject root;
    QJsonArray statList;
    root["isFacUp"] = false;
    root["statusList"] = statList;
    QJsonDocument doc(root);

    return doc.toJson();
}

void TextServer::_initFileData()
{
    QFile fileObj(_filename);
    bool isOpen = fileObj.open(QIODevice::ReadOnly | QIODevice::Text);
    if (!isOpen) {
        LOG_ERROR("Can't open file: " + _filename);
        return;
    }
    _fileData = fileObj.readAll();
}


