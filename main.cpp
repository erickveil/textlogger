#include <QCoreApplication>
#include "textserver.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QString filename;
    if (argc < 2 || QString(argv[1]) == "") { filename = "twoStationUp.json"; }
    else {  filename = argv[1]; }

    LOG_DEBUG("arg 0: " + QString(argv[0]));
    LOG_DEBUG("arg 1: " + QString(argv[1]));
    LOG_DEBUG("fle name: " + filename);


    TextServer server;
    server.init(filename);


    return a.exec();
}
